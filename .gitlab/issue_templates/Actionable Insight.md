<!-- Actionable insights must recommend an action that needs to take place. An actionable insight both defines the _insight_ and clearly calls out _action_ or next step required to improve based on the result of the research observation or data. Actionable insights are tracked over time and will include follow-up. Learn more in the handbook here: https://about.gitlab.com/handbook/engineering/ux/ux-research-training/research-insights/#actionable-insights -->

### Insight
<!-- Describe the insight itself: often the problem, finding, or observation. _What_ is currently happening? -->

### Supporting evidence
<!-- Describe _why_ the problem is happening, or more details behind the finding or observation. Try to include quotes or specific data collected. Feel free to link the Actionable insight from Dovetail here if applicable instead of retyping details. -->

### Action
<!--Describe the next step or action that needs to take place as a result of the research. The action should be clearly defined, achievable, and directly tied back to the insight. Make sure to use directive terminology, such as: conduct, explore, redesign, etc. _How_ do we take a step toward improving the experience? -->

### Resources
 <!--Add resources as links below or as related issues. -->

- **Dovetail link:** [{Paste URL here}](url)
- **Research issue link:** [{Paste URL here}](url)
- **Follow-up issue:** [{Paste URL here}](url)

### Tasks
- [ ] Assign this issue to the appropriate Product Manager, Product Designer, or UX Researcher.
- [ ] Add the appropriate `Group` (such as `~"group::source code"`) label to the issue.  This helps identify and track actionable insights at the group level.
- [ ] Link this issue back to the original research issue in the GitLab UX Research project and the Dovetail project.




/label ~"Actionable Insight"

